var Scriptsnew2 = (function() {
  var vars = {},
  methods = {};


  //MARCADORES DE GOOGLE MAPS
  var markers = [
    {
      name:'Patricia Ramos',
      lat:-12.126763,
      lon:-76.987968,
      photo:'profile-photo-small.png',
      type:0 //rojo
    },
    {
      name:'Brenda Araujo',
      lat:-12.128011,
      lon:-76.991815,
      photo:'profile.png',
      type:0 //rojo
    },
    {
      name:'Pepe Johns',
      lat:-12.128105,
      lon:-76.985797,
      photo:'profile.png',
      type:1 //gris
    }
  ];




  methods.init = function () {
    //CALENDARIO
    $('.datepicker2').datepicker();


    //CHECKBOX E INPUTS
    $('input').iCheck({
      checkboxClass: 'icheckbox_flat-red',
      radioClass: 'iradio_flat-red',
      increaseArea: '20%'
    });


    //SUBIR FOTO
    $(".btn-upload-product-picture2").on("change", function (evt) {
      var _this = this;
      var tgt = evt.target || window.event.srcElement,
          files = tgt.files;
      if (FileReader && files && files.length) {
          var fr = new FileReader();
          fr.onload = function () {
            $(_this).parent().prev('.load-img-here').attr('src', fr.result)
          }
          fr.readAsDataURL(files[0]);
      }
    });

    $(".btn-upload-product-picture3").on("change", function (evt) {
      var _this = this;
      var tgt = evt.target || window.event.srcElement,
          files = tgt.files;
      if (FileReader && files && files.length) {
          var fr = new FileReader();
          fr.onload = function () {
            $(_this).parent().parent().next('.load-img-here').attr('src', fr.result)
          }
          fr.readAsDataURL(files[0]);
      }
    });


    //ELIMINAR FOTO QUE SE IBA A SUBIR
    $(".remove-photo").on("click", function () {
      $(this).next('.load-img-here').attr('src','')
    });



    //CARGAR MAPA
    methods.maps(-12.126763, -76.987968);
  }




  methods.maps = function(lat, lon) {
    var mapOptions = {
        center: {lat:lat, lng:lon},
        zoom: 17
    },
    mapDiv = document.getElementById("map");

    if (mapDiv) {
      map = new google.maps.Map(mapDiv, mapOptions);

      //CARGAR MARCADORES
      methods.setMarkers(map);
    }
  }

  methods.setMarkers = function(map) {
    for (var i = 0; i < markers.length; i++) {
      var marker = markers[i],
      myLatlng = new google.maps.LatLng(marker.lat,marker.lon);
    	overlay = new CustomMarker(
    	  myLatlng,
    		map,
        marker.photo,
        marker.name,
        marker.type
    	);
    }
  }


  var CustomMarker = function (latlng, map, photo, name, type) {
  	this.latlng = latlng;
  	this.photo = photo;
  	this.setMap(map);
    this.name = name;
    this.type = type;
  }

  CustomMarker.prototype = new google.maps.OverlayView();

  CustomMarker.prototype.draw = function() {
  	var self = this;
  	var div = this.div;

  	if (!div) {
      //CREANDO ELEMENTO, IMAGEN Y BLOQUE TEXTO
  		div = this.div = document.createElement('div');
      var img = document.createElement('img');
      img.className = 'marker-photo';
      img.src = 'images/' + this.photo;
      div.appendChild(img);

      var span = document.createElement('span');
      span.className = 'marker-name';
      spanText = document.createTextNode(this.name);
      span.appendChild(spanText);
      div.appendChild(span);

      if (this.type == 1) {
        div.className = 'marker gray';
      } else {
        div.className = 'marker';
      }

  		var panes = this.getPanes();
  		panes.overlayImage.appendChild(div);
  	}

  	var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

  	if (point) {
  		div.style.left = (point.x - 10) + 'px';
  		div.style.top = (point.y - 20) + 'px';
  	}
  };

  CustomMarker.prototype.remove = function() {
  	if (this.div) {
  		this.div.parentNode.removeChild(this.div);
  		this.div = null;
  	}
  };

  CustomMarker.prototype.getPosition = function() {
  	return this.latlng;
  };


 return {
    methods : methods,
    vars : vars
  };

})();
