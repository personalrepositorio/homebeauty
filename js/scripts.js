var Buttons = (function () {
  'use strict';



  //////////////////////////
  //// VARIABLES PRIVADAS ////
  ////////////////////////////
  var vars = {},
  methods = {},
  productStatus,
  selectedRadioButtons,
  visiblePassword = false,
  visiblePassword1 = false,
  visiblePassword2 = false,
  visiblePassword3 = false;






  ////////////////////////////
  //// VARIABLES GLOBALES ////
  ////////////////////////////
  vars = {};







  //////////////////////////
  //// MÉTODOS PRIVADOS ////
  //////////////////////////









  //////////////////////////
  //// MÉTODOS PÚBLICOS ////
  //////////////////////////
  methods.btns = function () {

    //MOSTRAR PASSWORD
    $(document).on('click', '.show-password', function() {
      if ($(this).hasClass('one')) {
        visiblePassword1 = !visiblePassword1;
        visiblePassword = visiblePassword1;
      }
      if ($(this).hasClass('two')) {
        visiblePassword2 = !visiblePassword2;
        visiblePassword = visiblePassword2;
      }
      if ($(this).hasClass('three')) {
        visiblePassword3 = !visiblePassword3;
        visiblePassword = visiblePassword3;
      }

      if (visiblePassword) {
        $(this).parent().children('input[type="password"]').attr('type', 'text');
      } else {
        $(this).parent().children('input[type="text"]').attr('type', 'password');
      }
    });





    //FUNCIONAMIENTO ROLLOVER EN TABLAS
    $(".table tbody tr").on("click", function (e) {
      $(this).siblings('.tr-selected').removeClass('tr-selected');
      $(this).addClass('tr-selected');

      $(".filter-footer .product-filter_btn").addClass('filter_btn-visible');
    });
    $(document).on("mouseleave", ".main_content", function () {
      $(".filter-footer .product-filter_btn").removeClass('filter_btn-visible');
      $('.tr-selected').removeClass('tr-selected');
    });


    //ON OFF PARA EL ESTADO DEL PRODUCTO EN LAS TABLAS
    $(".table_radio-buttons").on("click", function (e) {
      e.stopPropagation();
      selectedRadioButtons = $(this);
      productStatus = selectedRadioButtons.attr('data-status');

      if (productStatus == '1') {
        $('#deactivate-product-modal').modal();
      } else if (productStatus == '0'){
        selectedRadioButtons.attr('data-status', '1').toggleClass('table_radio-buttons-on table_radio-buttons-off').closest('tr').removeClass('tr-disabled');
      }
    });
    $("#deactivate-product-modal .btn-change-status").on("click", function () {
      selectedRadioButtons.attr('data-status', '0').toggleClass('table_radio-buttons-on table_radio-buttons-off').closest('tr').addClass('tr-disabled');
    });



    //MOSTRAR IMAGEN ELEGIDA
    $(".btn-upload-product-picture").on("change", function (evt) {
      var _this = this;
      var tgt = evt.target || window.event.srcElement,
          files = tgt.files;
      if (FileReader && files && files.length) {
          var fr = new FileReader();
          fr.onload = function () {
            $(_this).parent().prev('.load-img-here').attr('src', fr.result)
          }
          fr.readAsDataURL(files[0]);
      }
    });



    //SHOW STYLES ON FOCUS
    $(document).on("focus", ".input-on-focus", function () {
      $(this).siblings('label').addClass('on');
    });
    $(document).on("blur", ".input-on-focus", function () {
      $(this).siblings('label').removeClass('on');
    });


    $(document).on('click', '.tab_a', function (e) {
      e.preventDefault()
      $(this).parent().children('.tab_a').removeClass('active');
      $(this).addClass('active').tab('show');
    });

$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
      //console.log(numFiles);
      $('.input-message').text(label);
      //console.log(label);
    });
  };








 return {
      methods : methods,
      vars : vars
  };


})();

var Init = (function (Modernizr, Buttons) {
  'use strict';


  ////////////////////////////
  //// VARIABLES PRIVADAS ////
  ////////////////////////////
  var vars = {},
  methods = {},
  searchByWord,
  table,
  place,
  nowTemp,
  now,
  newDate,
  sinceDate,
  untilDate;




  ////////////////////////////
  //// VARIABLES GLOBALES ////
  ////////////////////////////
  vars = {
    isMobile: false,
    ie9: false,
    ie10: false,
    ie11: false,
    facebookAppId: ''
  };






  //////////////////////////
  //// MÉTODOS PRIVADOS ////
  //////////////////////////







  //////////////////////////
  //// MÉTODOS PÚBLICOS ////
  //////////////////////////

  methods.ready = function () {

    //DETECCIÓN DE NAVEGADORES Y SI ES MOBILE
    if (window.Detectizr.device.type !== 'desktop' || Modernizr.mq('(max-width: 1280px)')) {
      vars.isMobile = true;
    }
    if(window.Detectizr.browser.name === 'ie') {
      if(window.Detectizr.browser.major === '9') {
        vars.ie9 = true;
      }
      if(window.Detectizr.browser.major === '10') {
        vars.ie10 = true;
      }
      if(window.Detectizr.browser.major === '11') {
        vars.ie11 = true;
      }
    }



    //CARGA DE VISTAS (TEMPORAL)
    var view = location.search;
    view = view.substring(1);
    $('.main_content').load(view + '.html', function () {

      //TABLAS
      methods.tables();

      //CALENDARIO
      methods.calendars();


      methods.formActions();


      Buttons.methods.btns();
    });


  };




  methods.load = function () {

  };




  methods.formActions = function () {
    //CHECKBOX Y RADIO PERSONALIZADOS
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-grey',
      radioClass: 'iradio_square-grey',
      increaseArea: '20%'
    });

    //MULTISELECT PERSONALIZADO
    $('.multiselect').multiselect({
      nonSelectedText:'',
      allSelectedText:'Todos'
    });

    //IMPEDIR INGRESO DE LETRAS EN INPUTS NUMÉRICOS
    $('.input-number').numeric();
  };




  methods.tables = function () {
    table = $('.datatable').DataTable({
      "paging":   true,
      "info":     false,
			"language": {
        "paginate": {
          "previous": "&#60",
          "next": "&#62;"
        }
			}
    });


    //FILTRAR POR CATEGORÍAS DENTRO DE LAS TABLAS
    $('.product-category').on( 'change', function () {
      table
        .columns( 2 )
        .search( this.value )
        .draw();
    } );


    //FILTRAR POR PALABRAS DENTRO DE LAS TABLAS
    $(".btn-filter-search").on("click", function () {
      searchByWord = $('.product-filter .product-name').val();
      table
        .search( searchByWord )
        .draw();
    });
  };



  methods.calendars = function () {
    nowTemp = new Date();
    now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    sinceDate = $('#product_datepicker-since').datepicker({
      onRender: function(date) {
        return date.valueOf() < now.valueOf() ? 'disabled' : '';
      }
    }).on("changeDate", function (ev) {
      if (ev.date.valueOf() > untilDate.date.valueOf()) {
        newDate = new Date(ev.date);
        newDate.setDate(newDate.getDate() + 1);
        untilDate.setValue(newDate);
      }
      $('#product_datepicker-since').datepicker('hide');
    }).data('datepicker');

    untilDate = $('#product_datepicker-until').datepicker({
      onRender: function(date) {
        return date.valueOf() <= sinceDate.date.valueOf() ? 'disabled' : '';
      }
    }).on("changeDate", function (ev) {
      $('#product_datepicker-until').datepicker('hide');
    }).data('datepicker');
  };





 return {
      methods : methods,
      vars : vars
  };


})(Modernizr, Buttons);




//CUANDO HA CARGADO EL DOM
$(document).ready(Init.methods.ready);


//CUANDO HA CARGADO DOM, IMÁGENES, ETC
$(window).load(Init.methods.load);
